<?php
class Views {
    static $path = '/views/';
    static function view( $view ) {
        require_once( $_SERVER['DOCUMENT_ROOT'] . self::$path . $view . '.php');
    }
    static function redirect( $view ) {
       header('Location: ' . $view);
    }
    function doc() {
        return (file_get_contents("Views.doc.txt"));
    }
}
?>