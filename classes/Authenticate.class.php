<?php
require_once 'classes/Database.class.php';
require_once 'classes/User.class.php';

class Authenticate {
    public function auth($login, $password) {
        
        $db = new Database;
        $conn = $db->connection();
        $prep = $conn->prepare("select * from `users` where `login` like ?");
        $sth = $prep->execute( array($login) );
        
        if ($prep->rowCount() > 0) {
            $prep = $conn->prepare("select * from `users` where `login` like ? and `password` like ?");
            $sth = $prep->execute( array($login, hash('whirlpool', $password)) );
            if ($prep->rowCount() > 0) {
                $user = $prep->fetch(PDO::FETCH_ASSOC);
                session_start();
                $_SESSION['id_user'] = $user['id'];
                $_SESSION['login'] = $user['login'];
                $_SESSION['logged_in'] = true;
                return ('Success');
            }
            else {
                return ('WrongPassword');
            }
        }
        else {
            return ('WrongLogin');
        }
    }
    public function register( array $args ) {
        if (array_key_exists('login', $args) &&
            array_key_exists('pass', $args) &&
            array_key_exists('name', $args) &&
            array_key_exists('surname', $args)) {
            $db = new Database;
            $conn = $db->connection();
            $prep = $conn->prepare("select * from `users` where `login` like ?");
            $sth = $prep->execute( array($args['login']) );
            
            if ($prep->rowCount() == 0) {
                
                $prep = $conn->prepare("insert into `users` (login, password, name, surname)values(?, ?, ?, ?)");
                $sth = $prep->execute( array($args['login'], hash('whirlpool', $args['pass']), $args['name'], $args['surname']) );
                if ($sth) {
                    return ('Success');
                }
                else {
                    return ($prep->errorCode());
                }
            }
            else {
                return ('LoginExists');
            }
        }
    }
    public function deauth() {
        session_start();
        $_SESSION = array();
        session_destroy();
    }
    public function isLoggedIn() {
        session_start();
        if (!empty($_SESSION['id_user']) && !empty($_SESSION['login']) && !empty($_SESSION['logged_in']) && $_SESSION['logged_in'] === true)
            return (true);
        else
            return (false);
    }
    public function getUser() {
        session_start();
        $user = new User($_SESSION['id_user']);
        return ($user->getDates());
    }
}
?>