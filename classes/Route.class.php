<?php
class Route {
    static function get( $url, $func ) {
        if ($_SERVER['REQUEST_URI'] == $url) {
            call_user_func($func);
            exit();
        }
    }
    function doc() {
        return (file_get_contents("Route.doc.txt"));
    }
}
?>