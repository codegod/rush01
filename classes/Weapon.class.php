<?php
class Weapon {
    private $_name;
    private $_charge;
    private $_short_range;
    private $_middle_range;
    private $_long_range;
    private $_effect_zone;
    public function __construct( array $args ) {
        if (
            array_key_exists('name', $args)
            && array_key_exists('charge', $args)
            && array_key_exists('short_range', $args) 
            && array_key_exists('middle_range', $args)
            && array_key_exists('long_range', $args) 
            && array_key_exists('effect_zone', $args)
            ) {
            $this->_name            = $args['name'];
            $this->_charge          = $args['charge'];
            $this->_short_range     = $args['short_range'];
            $this->_middle_range    = $args['middle_range'];
            $this->_long_range      = $args['long_range'];
            $this->_effect_zone     = $args['effect_zone'];
        }
    }
    public function getDates() {
        return ( array(
            'name' => $this->_name,
            'charge' =>$this->_charge,
            'short_range' => $this->_short_range,
            'middle_range' => $this->_middle_range,
            'long_range' => $this->_long_range,
            'effect_zone' => $this->_effect_zone
            ) );
    }
    function doc() {
        return (file_get_contents("Weapon.doc.txt"));
    }
}
?>