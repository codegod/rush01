<?php
class Ship {
    private $_name;
    private $_size;
    private $_equiv;
    private $_hullpoints;
    private $_pp;
    private $_speed;
    private $_handling;
    private $_shield;
    private $_weapons;
    public function __construct( array $args ) {
        if (
            array_key_exists('name', $args)
            && array_key_exists('size', $args)
            && array_key_exists('equiv', $args) 
            && array_key_exists('hullpoints', $args)
            && array_key_exists('powerpoints', $args) 
            && array_key_exists('speed', $args)
            && array_key_exists('handling', $args) 
            && array_key_exists('shield', $args)
            && array_key_exists('weapons', $args)
            ) {
            $this->_name        = $args['name'];
            $this->_size        = $args['size'];
            $this->_equiv       = $args['equiv'];
            $this->_hullpoints  = $args['hullpoints'];
            $this->_pp          = $args['powerpoints'];
            $this->_speed       = $args['speed'];
            $this->_handling    = $args['handling'];
            $this->_shield      = $args['shield'];
            $this->_weapons     = $args['weapons'];
        }
    }
    public function getDates() {
        return ( array(
            'name' => $this->_name,
            'size' =>$this->_size,
            'equiv' => $this->_equiv,
            'hullpoints' => $this->_hullpoints,
            'powerpoints' => $this->_pp,
            'speed' => $this->_speed,
            'handling' => $this->_handling,
            'shield' =>$this->_shield,
            'weapons' => $this->_weapons
            ) );
    }
    function doc() {
        return (file_get_contents("Ship.doc.txt"));
    }
}
?>