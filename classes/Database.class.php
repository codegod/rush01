<?php
class Database {
    protected $_login;
    protected $_pass;
    protected $_database;
    public function __construct() {
        $this->_login       = 'constantinc';
        $this->_pass        = '';
        $this->_database    = 'db_game';
    }
    public function getQuery( $sql ) {
        try {
            $dbh = new PDO('mysql:host=localhost;dbname='.$this->_database, $this->_login, $this->_pass);
            $stmt   = $dbh->prepare($sql);
            $result = $stmt->execute();
            $data   = $stmt->fetch(PDO::FETCH_ASSOC);
            $dbh = null;
            return ($data);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            return(false);
        }
    }
    public function getAllQuery( $sql ) {
        try {
            $dbh = new PDO('mysql:host=localhost;dbname='.$this->_database, $this->_login, $this->_pass);
            $stmt   = $dbh->prepare($sql);
            $result = $stmt->execute();
            $i = 0;
            while ($res  = $stmt->fetch(PDO::FETCH_ASSOC))
                $data[$i++] = $res;
            $dbh = null;
            return ($data);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            return(false);
        }
    }
    public function execQuery( $sql ) {
        try {
            $dbh = new PDO('mysql:host=localhost;dbname='.$this->_database, $this->_login, $this->_pass);
            $stmt   = $dbh->prepare($sql);
            $result = $stmt->execute();
            $dbh = null;
            if ($result)
                return (true);
            else
                return (false);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            return(false);
        }
    }
    public function connection() {
        try {
            $dbh = new PDO('mysql:host=localhost;dbname='.$this->_database, $this->_login, $this->_pass);
            return ($dbh);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            return (false);
        }
    }
}
?>