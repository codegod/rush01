<?php
$PageTitle = "Login";
$includes = array(
    'style'
    );
$extraIncludes = array('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
include_once 'views/header.php';
?>
    
  <div class="login-box">
    <div class="lb-header">
      <a href="#" class="active" id="login-box-link">Login</a>
      <a href="#" id="signup-box-link">Sign Up</a>
    </div>
    
    <form class="email-login"  id="email-login" method='POST'>
      <div class="u-form-group">
        <input type="text" placeholder="Login" name='in_login'/>
         <p id='erlogin' class="error"></p>
      </div>
      <div class="u-form-group">
        <input type="password" placeholder="Password" name='in_pass'/>
        <p  id='erpass' class="error"></p>
        <input type="hidden" name="scope" value='032666'/>
      </div>
      <div class="u-form-group">
        <button id='loginbtn'>Log in</button>
      </div>
      <div class="u-form-group">
        <a href="#" class="forgot-password">Forgot password?</a>
      </div>
    </form>
    
    
    <form class="email-signup"  id="email-signup" method='POST'>
      <div class="u-form-group">
        <input type="text" placeholder="Login" name='up_login'/>
        <p id='erlogin1' class="error"></p>
      </div>
      <div class="u-form-group">
        <input type="text" placeholder="Name" name='up_name'/>
      </div>
      <div class="u-form-group">
        <input type="text" placeholder="Surname" name='up_surname'/>
      </div>
      <div class="u-form-group">
        <input type="password" placeholder="Password"  name='up_pass' id='pass'/>
      </div>
      <div class="u-form-group">
        <input type="password" placeholder="Confirm Password"  name='up_repass' id='repass'/>
        <p id='erpass1' class="error"></p>
        <input type="hidden" name="scope" value='032555'/>
      </div>
      <div class="u-form-group">
        <button id='signupbtn'>Sign Up</button>
      </div>
    </form>
  </div>
  
     <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

   <script type="text/javascript">
   
   
    
   
  $(".email-signup").hide();
   
    $("#signup-box-link").click(function(){
   
    
        
      $(".email-login").fadeOut(100);
      $(".email-signup").delay(100).fadeIn(100);
      $("#login-box-link").removeClass("active");
      $("#signup-box-link").addClass("active");
      $(".login-box").css('height', '470');
    });
    
    $("#login-box-link").click(function(){
        
        
      $(".email-login").delay(100).fadeIn(100);;
      $(".email-signup").fadeOut(100);
      $("#login-box-link").addClass("active");
      $("#signup-box-link").removeClass("active");
      $(".login-box").css('height', '380');
    });
    
      $('#signupbtn').click(function(e){
        e.preventDefault();
        $('p.error').text('');
        if ($('#pass').val() === "" && $('#repass').val() === "")
          $('#erpass1').text('Password not be empty!');
        else if (($('#pass').val() != $('#repass').val()))
          $('#erpass1').text('Password not match!');
        else
          $.ajax({
              type: "POST",
              url: "/process",
              data: $("#email-signup").serialize(),
              success: function (result) {
              if (result == 'Success')
                  window.location.href = '/';
              else if (result == 'LoginExists')
                $('#erlogin1').text('Login already exists!');
            }
          });
        
    });  
    
    $('#loginbtn').click(function(e){
        e.preventDefault();
        $('p.error').text('');
        $.ajax({
            type: "POST",
            url: "/process",
            data: $("#email-login").serialize(),
            success: function (result) {
            if (result == 'Success')
                window.location.href = '/';
            else if (result == 'WrongLogin')
              $('#erlogin').text('Wrong Login!');
            else if (result == 'WrongLogin')
              $('#erlogin').text('Wrong Password!');
          }
        });
        
    });  
    
    
    
    </script>
<?php
require_once 'views/footer.php';
?>