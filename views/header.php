<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo isset($PageTitle) ? $PageTitle : 'Battle-Ship Game'; ?></title>
    <link rel="shortcut icon" type="image/png" href="/assets/images/favicon.png"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,700">
    <link rel="stylesheet" href="/assets/css/menu.css">
    <style type="text/css">
    <?php
    if (isset($style))
        echo $style; 
    ?>
    </style>
<?php 
    if (isset($includes)) {
        foreach ($includes as $include) {
            echo "\t<link rel='stylesheet' href='/assets/css/" . $include . ".css' type='text/css' />\n";
        }
    }
    if (isset($extraIncludes)) {
        foreach ($extraIncludes as $include) {
            echo "\t<link rel='stylesheet' href='" . $include . "' type='text/css' />\n";
        }
    }
    require_once 'classes/Authenticate.class.php';
    $auth = new Authenticate;
?>
</head>
<body>
    <nav id="primary_nav_wrap">
<ul>
  <li><a href="/">Home</a></li>
  <li><a href="/login">Members</a>
    <ul>
      <li><a href="/login">Login</a></li>
    </ul>
  </li>
  <?php
  if ($auth->isLoggedIn()) {
      $user = $auth->getUser();
  ?>
  <li><a href="/users"><?php echo $user['name']. ' ' . $user['surname']; ?></a>
  <ul>
      <li><a href="/game">Play Game</a></li>
      <li><a href="/users">Settings</a></li>
      <?php if (isset($game)) {?>
      <li><a href="/add">Add Friend</a></li>
      <?php } ?>
      <li><a href="/logout">Logout</a></li>
    </ul>
  </li>
<?php
}
?>
  
 
</ul>
</nav>