<?php
require_once 'classes/Authenticate.class.php';

if ($_POST['scope'] == '032666' && $_POST['in_login'] != '' && $_POST['in_pass'] != '') {
    $auth = new Authenticate;
    die ($auth->auth($_POST['in_login'], $_POST['in_pass']));
}
elseif ($_POST['scope'] == '032555' && $_POST['up_login'] != '' && $_POST['up_pass'] != '' && $_POST['up_name'] != '' && $_POST['up_surname'] != '') {
    $auth = new Authenticate;
    die ( $auth->register( array(
            'login' => $_POST['up_login'],
            'pass' => $_POST['up_pass'],
            'name' => $_POST['up_name'], 
            'surname' => $_POST['up_surname']) ) );
}
else {
    die('No Access');
}


?>