<?php
require_once 'classes/Authenticate.class.php';
require_once 'classes/Database.class.php';



$auth = new Authenticate;
$user = $auth->getUser();
$color = $user['color'];
$uid = $user['id'];
$style = ".user$uid {\n\tbackground-color:$color;\n}\n";
$sql = "select g_match.friend_id as friend_id, users.color as color 
    from g_match inner join users on users.id = g_match.friend_id 
    where g_match.user_id = $uid;";
    $db = new Database;
    $res = $db->getAllQuery($sql);
    
foreach ($res as $uss) {

    $usid = $uss['friend_id'];
    $color = $uss['color'];
    $style .= ".user$usid {\n\tbackground-color:$color;\n}\n";

}

$PageTitle = 'Let\'s play - BattleShips';
$includes = array('game');
$game = 1;
require_once 'header.php';
$n = 100;
$m = 150;

?>

<input type="hidden" name="maxx" id='maxx' value='<?php echo $n; ?>'/>
<input type="hidden" name="maxy" id='maxy' value='<?php echo $m; ?>'/>
<input type="hidden" name="uid" id='uid' value='<?php echo $uid; ?>'/>
<div>
    <span></span>
</div>
    <table border=1>
        <?php
        for($i = 1; $i <= $n; $i++){ ?>
        <tr>
            <?php for($j = 1; $j <= $m; $j++){ ?><td id='<?php echo $i . '_' . $j;?>'></td><?php } ?>
        </tr>
        <?php
        }
        ?>
    </table>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript">
    
    var isActive = true;

    $().ready(function () {
        pollServer();
    });
    
    function pollServer()
    {
        if (isActive)
        {
            window.setTimeout(function () {
                    
                $.ajax({
                    url: '/pool',
                    type: "POST",
                        data: {'uid':$('#uid').val()},
                    success: function (result) {
                        if (result != 'false')
                            {
                               var res = $.parseJSON(result);
                               for (var r = 0; r < res.length; r++) {
                                   $('.user' + res[r].friend_id).attr('class', '');
                                   $('#'+res[r].posy+'_'+res[r].posx).attr('class', 'user' + res[r].friend_id);
                               }
                                
                            }
                     pollServer();
                    },
                    error: function () {
                        //ERROR HANDLING
                        pollServer();
                    }});
            }, 2500);
        }
    }
    
      $(document).keydown( function(e) {
            var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
            e.preventDefault();
            var el = $('.user' + $('#uid').val()).attr('id');
            var sp = el.split('_');
            var move;
            if (key == 37){
                if (sp[1]-1 == 0)
                    alert('Ai iesit inafara!');
                else {
                    $('.user' + $('#uid').val()).attr('class', '');
                    move = '#'+sp[0]+'_'+(sp[1]-1);
                }
            }
            if (key == 38){
                if (sp[0]-1 == 0)
                    alert('Ai iesit inafara!');
                else {
                    $('.user' + $('#uid').val()).attr('class', '');
                    move = '#'+(sp[0]-1)+'_'+sp[1];
                }
            }
            if (key == 39){
                if (parseInt(sp[1])+1 == parseInt($('#maxy').val()) + 1)
                    alert('Ai iesit inafara!');
                else {
                    $('.user' + $('#uid').val()).attr('class', '');
                    move = '#'+sp[0]+'_'+(parseInt(sp[1])+1);
                }
            }
            if (key == 40){
                if (parseInt(sp[0])+1 == parseInt($('#maxx').val()) + 1)
                    alert('Ai iesit inafara!');
                else {
                    $('.user' + $('#uid').val()).attr('class', '');
                    move = '#'+(parseInt(sp[0])+1)+'_'+sp[1];
                }
            }
            $(move).attr('class', 'user' + $('#uid').val());
            
            $.ajax({
                url: '/pull',
                type: "POST",
                    data: {'uid':$('#uid').val(), 'posx': (move.substr(1, move.length)).split('_')[1], 'posy': (move.substr(1, move.length)).split('_')[0]},
                success: function (result) {
                },
                error: function () {
     
            }});
        });
    </script>
<?php
require_once 'footer.php';
?>