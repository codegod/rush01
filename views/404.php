<?php
$PageTitle = 'Error 404 - Not found';
$includes = array('404');
require_once 'header.php';
?>
    <h1>404</h1>
    <p>Oops! Something is wrong.</p>
    <a class="button" href="/"><i class="icon-home"></i> Go back in initial page, is better.</a>
<?php
require_once 'footer.php';
?>