<?php
require_once 'classes/Route.class.php';
require_once 'classes/Views.class.php';
require_once 'classes/Authenticate.class.php';

Route::get('/', function() {
    Views::view('default');
});

Route::get('/game', function() {
    $auth = new Authenticate;
    if ($auth->isLoggedIn())
        Views::view('game');
    else
        Views::redirect('/');
});

Route::get('/process', function() {
    Views::view('process');
});

Route::get('/logout', function() {
    $auth = new Authenticate;
    $auth->deauth();
    Views::redirect('/');
});

Route::get('/pool', function() {
    Views::view('pool');
});

Route::get('/pull', function() {
    Views::view('pull');
});

Route::get('/login', function() {
    $auth = new Authenticate;
    if (!$auth->isLoggedIn())
        Views::view('service/login');
    else
        Views::redirect('/');
});

Views::view('404');

?>